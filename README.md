# Ionic Signature Pad Demo
This is a barebones [Ionic](http://ionicframework.com/docs/) application for demonstrating the signature pad built in my blog post [here](http://www.jacklewis.me/blog/ionic_signature.html).

## How to run the app
[Follow the instructions](https://ionicframework.com/docs/intro/installation/) to install ionic
Run ```npm install``` to make sure the libraries you need are installed

### In the browser
```
ionic serve
```

### On mobile
```
ionic cordova run android
```
or
```
ionic cordova run ios
```