import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, ViewController } from 'ionic-angular';

import { SignaturePad } from 'angular2-signaturepad/signature-pad';


@IonicPage()
@Component({
  selector: 'page-signature-pad',
  templateUrl: 'signature-pad.html',
})
export class SignaturePadPage {
  // Get the dom elements
  @ViewChild(SignaturePad) signaturePad: SignaturePad;
  @ViewChild('pad', { read: ElementRef }) container: ElementRef;

  // Initial sizes for the canvas
  // Complete list of config options here - https://github.com/szimek/signature_pad
  private options = {
    'minWidth': 5,
    'canvasWidth': 500,
    'canvasHeight': 300
  }

  constructor( public viewCtrl: ViewController ) {}

  ionViewDidLoad() {
    // When the page has finished loading, resize the canvas to fit the screen
    this.signaturePad.set( 'canvasWidth', this.container.nativeElement.offsetWidth );
    this.signaturePad.set( 'canvasHeight', this.container.nativeElement.offsetHeight );  
  }

  save() {
    // Get the image of the signature as a base64 encoded string
    const base64Img = this.signaturePad.toDataURL();

    // Close the modal and pass the signature back
    this.viewCtrl.dismiss({ signature: base64Img });
  }

  cancel() {
    this.viewCtrl.dismiss({});
  }

}
